# Messages

## `response-type`

This determines what kind of message the object contains. It can be:

- `charge-success`
- `charge-form-error`
- `charge-processor-error`

## `charge-success`

- `created-at`
- `amount`
- `description`
- `receipt-email`

## `charge-form-error`

- `errors`: A list of form errors from the form parser.

## `charge-processor-error`

- `payment-error-class`
  - `transient`: the request can be resubmitted as-is and may succeed.
  - `retry-with-modification` - the request can't be processed as-is.
  - `duplicate-transaction` - the request has already been processed.
  - `local-configuration` - the application configuration is invalid.
  - `remote-system` - Stripe had a system error. The request may be
      retriable.
    - `stop` - This request is completely invalid.
- `message` - The error message from the processor.
- `error-code` - The error code from the processor if the error was
    with the card information or charge processing.
