((nil . (
            (projectile-project-compilation-cmd . "cabal run site-builder clean && cabal run site-builder build")
            (projectile-project-compilation-dir . "/")
            )
     )
    (purescript-mode .
        (
            (projectile-project-compilation-cmd . "npm --color=false run compile:debug")
            )
        )
    (js2-mode .
        (
            (projectile-project-compilation-cmd . "npm --color=false run compile:debug")
            )
        )
    (haskell-mode .
        (
            (projectile-project-compilation-cmd . #'haskell-process-cabal-build))
        )
    )
