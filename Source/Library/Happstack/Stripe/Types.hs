{-# OPTIONS_GHC -fno-warn-orphans #-}

module Happstack.Stripe.Types (
    module Happstack.Stripe.Types.Hooks,
    module Happstack.Stripe.Types.Chargeable,
    module Happstack.Stripe.Types.PaymentForm,
    module Happstack.Stripe.Types.Logging,
    module Happstack.Stripe.Types.Messages,
    module Happstack.Stripe.Types.TxnId,
    StripeKey(..),
    StripePublishKey(..),
    Description(..),
    spkString
    ) where

import Happstack.Stripe.Prelude
import Happstack.Stripe.Types.Hooks
import Happstack.Stripe.Types.Chargeable
import Happstack.Stripe.Types.PaymentForm
import Happstack.Stripe.Types.Logging
import Happstack.Stripe.Types.Messages
import Happstack.Stripe.Types.TxnId
import Data.ByteString.Char8 as C8
import Web.Stripe.Client

newtype StripePublishKey = StripePublishKey StripeKey deriving (Eq, Data, Ord, Read, Show)
makePrisms ''StripePublishKey

spkString ∷ Getter StripePublishKey String
spkString =
    let
        skToString (StripeKey k) = C8.unpack k
    in
      to (skToString ∘ view _StripePublishKey)
