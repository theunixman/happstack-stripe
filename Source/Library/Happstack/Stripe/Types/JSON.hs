{-|
Module         : JSON
Description:   : Specializations of the default Aeson generic encoding.
Copyright      : ©2016
License        : GPL-3
Maintainer     : Evan Cofsky <evan@theunixman.com>
Stability      : experimental
Portability    : POSIX
-}

module Happstack.Stripe.Types.JSON (
    module Data.Aeson,
    stripFieldPrefix,
    encodingOptions,
    messageToEncoding
    ) where

import Happstack.Stripe.Prelude
import Data.Aeson
import Data.Aeson.Types
import Data.List (stripPrefix)
import Data.Maybe (fromMaybe, mapMaybe)

stripFieldPrefix ∷ [String] → String → String
stripFieldPrefix pfxs label =
    let
        allpfxs = concat [pfxs, map (\p -> "_" ++ p) pfxs]
        lbl = firstOf folded $ mapMaybe (flip stripPrefix label) allpfxs
    in
        fromMaybe label lbl

encodingOptions ∷ [String] → Options
encodingOptions fieldPrefix = defaultOptions
    {
        fieldLabelModifier = camelTo2 '-' . stripFieldPrefix fieldPrefix,
        constructorTagModifier = camelTo2 '-',
        allNullaryToStringTag = True,
        omitNothingFields = False,
        sumEncoding = TaggedObject "response-type" "response"
    }

messageToEncoding ::
    ∀ a.
    (GToEncoding Zero (Rep a), Generic a) ⇒
    [String] → a → Encoding
messageToEncoding =
        genericToEncoding ∘ encodingOptions

-- | Make sure we can JSON encode and decode NFCText
instance ToJSON NFCText where
    toEncoding = messageToEncoding [""]

instance FromJSON NFCText where
    parseJSON (Data.Aeson.String t) = return ∘ review strict $ t
    parseJSON invalid = typeMismatch "Expected JSON String for NFCText" invalid
