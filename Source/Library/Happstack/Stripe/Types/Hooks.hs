{-# Language TemplateHaskell #-}

module Happstack.Stripe.Types.Hooks where

import Happstack.Stripe.Prelude
import Happstack.Stripe.Types.PaymentForm
import Happstack.Stripe.Types.Messages
import Happstack.Stripe.Types.TxnId

type ChargeRequestHook = TxnId → PaymentForm → IO ()
type ChargeSuccessHook = TxnId → Charge → IO ()
type PaymentFormErrorHook = TxnId → PaymentFormError → IO ()
type ChargeProcessorErrorHook = TxnId → ProcessorError → IO ()

callHooks ∷ TxnId → a → [(TxnId → a → IO ())] → IO ()
callHooks t a = mapM_ (\f → f t a)
