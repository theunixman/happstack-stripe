module Happstack.Stripe.Types.Messages.PaymentErrorClass where

import Happstack.Stripe.Prelude
import Happstack.Stripe.Types.Messages.Retryable
import Happstack.Stripe.Types.JSON

-- | The general instructions for the caller on what to do.
data PaymentErrorClass =
    -- | The request can be retried as is.
    Transient |

    -- | The request had inalid data or the card couldn't be charged.
    RetryWithModification |

    -- | This is a duplicate of another transaction.
    DuplicateTransaction |

    -- | A local configuration error will prevent any calls from succeeding.
    --
    -- This could be credentials, file permissions, database access, etc.
    LocalConfiguration |

    -- | A remote system error will prevent any calls from succeeding.
    --
    -- This would be a Stripe issue.
    RemoteSystem |

    -- | This request should not be resubmitted.
    Stop

    deriving (Show, Eq, Ord, Generic)
makePrisms ''PaymentErrorClass

pecHttpErrorCode ∷ Getter PaymentErrorClass Int
pecHttpErrorCode = to (\case
                              Transient → 429
                              RetryWithModification → 400
                              DuplicateTransaction → 409
                              LocalConfiguration → 500
                              RemoteSystem → 503
                              Stop → 404)

pecIsRetryable :: Getter PaymentErrorClass Retryable
pecIsRetryable = to (\case
                            Transient -> Retryable
                            RetryWithModification -> RetryableWithModification
                            DuplicateTransaction -> NotRetryable
                            LocalConfiguration -> NotRetryable
                            RemoteSystem -> Retryable
                            Stop -> NotRetryable)

instance ToJSON PaymentErrorClass where
    toEncoding = messageToEncoding [""]
