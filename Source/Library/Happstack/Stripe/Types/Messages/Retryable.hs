module Happstack.Stripe.Types.Messages.Retryable (
    Retryable(..)
    ) where

import Happstack.Stripe.Prelude hiding (string)
import Data.Aeson
import Data.Aeson.Encoding

-- | Whether a transaction can be resubmitted and how.
data Retryable =
    Retryable |
    RetryableWithModification |
    NotRetryable
    deriving (Show, Eq, Ord, Generic)

instance ToJSON Retryable where
    toEncoding r =
        string $ case r of
                     Retryable → "retryable"
                     RetryableWithModification → "with_modification"
                     NotRetryable → "not_retryable"
