module Happstack.Stripe.Types.Messages.ChargeResult where

import Happstack.Stripe.Prelude
import Happstack.Stripe.Types.JSON
import Happstack.Stripe.Types.Logging
import Happstack.Stripe.Types.Messages.ChargeInfo
import Happstack.Stripe.Types.Messages.ProcessorError
import Happstack.Stripe.Types.Messages.FormError
import Happstack.Stripe.Types.Messages.Retryable
import Happstack.Stripe.Types.Messages.Classes

import Happstack.Server (ToMessage(..), Response(..), toResponseBS)
import Data.Time.Clock
import qualified Data.Text as T
import Control.Applicative
import Data.Maybe (fromMaybe)
import Happstack.Stripe.Types.Messages.ChargeInfo

data ChargeResult =
    ChargeSuccess
    {
        _csSuccess ∷ ChargeInfo
    } |
    ChargePaymentFormError
    {
        _csRetryable ∷ Retryable,
        _csFormError ∷ PaymentFormError
    } |
    ChargeProcessorError
    {
        _csRetryable ∷ Retryable,
        _csProcessorError ∷ ProcessorError
    } deriving (Show, Eq, Ord, Generic)
makeLenses ''ChargeResult
makePrisms ''ChargeResult

instance ToJSON ChargeResult where
    toEncoding = messageToEncoding ["cs"]

crHttpResponseCode ∷ Getter ChargeResult Int
crHttpResponseCode =
    to (\cr →
           ((view pmHttpResponseCode <$> cr ^? _ChargePaymentFormError) <|>
            (view pmHttpResponseCode <$> cr ^? _ChargeProcessorError) <|>
            (view pmHttpResponseCode <$> cr ^? _ChargeSuccess)) ^?! _Just)

instance ToMessage ChargeResult where
    toContentType = const "application/json"
    toMessage = encode
    toResponse  msg =
        let
            rc r  = r {rsCode = msg ^. crHttpResponseCode}
        in
          rc $ toResponseBS (toContentType msg) (toMessage msg)

fromFormError :: [String] -> ChargeResult
fromFormError errs =
    let
        pfe = PaymentFormError $ map (review utf8) errs
    in
        ChargePaymentFormError (pfe ^. peRetryable) pfe

fromCharge :: Charge -> ChargeResult
fromCharge chg = ChargeSuccess $ ChargeInfo
    (chargeCreated chg)
    (T.pack $ showAmount (chargeCurrency chg) (chargeAmount chg))
    (fromDescription <$> chargeDescription chg)
    (review strict <$> chargeReceiptEmail chg)
