module Happstack.Stripe.Types.Messages.FormError where

import Happstack.Stripe.Prelude
import Happstack.Stripe.Types.JSON
import Happstack.Stripe.Types.Messages.Classes
import Happstack.Stripe.Types.Messages.PaymentErrorClass
import Happstack.Stripe.Types.Logging

data PaymentFormError = PaymentFormError
    {
        _pfeErrors :: [NFCText]
    } deriving (Show, Eq, Ord, Generic)
makeLenses ''PaymentFormError

instance Monoid PaymentFormError where
    mempty = PaymentFormError []
    a `mappend` b = PaymentFormError $ (a ^. pfeErrors) ⊕ (b ^. pfeErrors)

instance ToJSON PaymentFormError where
    toEncoding = messageToEncoding ["pfe"]

instance IsPaymentError PaymentFormError where
    peErrorClass = to $ const RetryWithModification

instance IsPaymentMessage PaymentFormError

instance ToMessage PaymentFormError where
    toContentType = const "application/json"
    toMessage = encode

instance Loggable PaymentFormError where
    toLogMessage err = LogMessage ["Form errors:" ^. re utf8] ⊕ LogMessage (err ^. pfeErrors)
