module Happstack.Stripe.Types.Messages.ChargeInfo where

import Happstack.Stripe.Prelude
import Happstack.Stripe.Types.Messages.Classes
import Data.Time.Clock

data ChargeInfo =
    ChargeInfo
    {
        _ciCreatedAt :: UTCTime,
        _ciAmount :: Text,
        _ciDescription :: Maybe NFCText,
        _ciReceiptEmail :: Maybe NFCText
    } deriving (Show, Eq, Ord, Generic)
makeLenses ''ChargeInfo

instance ToJSON ChargeInfo where
    toEncoding = messageToEncoding ["ci"]

instance IsPaymentMessage ChargeInfo where
    pmHttpResponseCode = to (const 200)

instance ToMessage ChargeInfo where
    toContentType = const "application/json"
    toMessage = encode

fromDescription ∷ Description → NFCText
fromDescription (Description t) = review strict t
