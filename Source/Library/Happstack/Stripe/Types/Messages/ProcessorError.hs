module Happstack.Stripe.Types.Messages.ProcessorError where

import Happstack.Stripe.Prelude hiding (string)
import Happstack.Stripe.Types.Logging
import Happstack.Stripe.Types.Messages.Classes
import Happstack.Stripe.Types.Messages.FormError
import Happstack.Stripe.Types.Messages.PaymentErrorClass
import Web.Stripe.Error (StripeErrorCode(..))
import qualified Data.Text as T
import Data.Maybe (fromMaybe)

newtype ErrorCode = ErrorCode StripeErrorCode deriving (Show, Eq, Ord, Generic)

deriving instance Eq StripeErrorCode
deriving instance Ord StripeErrorCode

instance ToJSON ErrorCode where
    toEncoding (ErrorCode e) =
        case e of
            IncorrectNumber → string "Incorrect card number."
            InvalidNumber → string "Invalid card number."
            InvalidExpiryMonth → string "Invalid expiration month."
            InvalidExpiryYear → string "Invalid expiration year."
            InvalidCVC → string "Invalid CVC."
            ExpiredCard → string "Card expired."
            IncorrectCVC → string "CVC doesn't match card."
            IncorrectZIP → string "Incorrect ZIP code."
            CardDeclined → string "Card declined."
            Missing → string "Missing information."
            ProcessingError → string "Error processing transaction."
            RateLimit → string "Please try again."
            UnknownError → string "Unknown error."

data ProcessorError = ProcessorError
    {
        _preErrorClass :: PaymentErrorClass,
        _preMessage :: NFCText,
        _preErrorCode :: ErrorCode
    } deriving (Show, Eq, Ord, Generic)
makeLenses ''ProcessorError

instance IsPaymentError ProcessorError where
    peErrorClass = to _preErrorClass

instance IsPaymentMessage ProcessorError

instance Loggable ProcessorError where
    toLogMessage pe = mconcat
        [
            toLogMessage $ (show $ pe ^. preErrorClass) ^. re utf8,
            toLogMessage $ pe ^. preMessage,
            toLogMessage $ (show $ pe ^. preErrorCode) ^. re utf8
        ]

instance ToJSON ProcessorError where
    toEncoding =  messageToEncoding ["_pre"]

instance ToMessage ProcessorError where
    toContentType = const "application/json"
    toMessage = encode
    toResponse pe =
        let
            rc r = r {rsCode = pe ^. peHttpResponseCode}
        in
          rc $ toResponseBS (toContentType pe) (toMessage pe)

fromStripeErrorType ∷ StripeErrorType → PaymentErrorClass
fromStripeErrorType = \case
    InvalidRequest -> RetryWithModification
    APIError -> RemoteSystem
    CardError -> RetryWithModification
    ConnectionFailure -> RemoteSystem
    ParseFailure -> RetryWithModification
    UnknownErrorType -> RemoteSystem

-- | Some 'StripeError's are actually 'PaymentFormError's.
fromStripeError ∷ StripeError → Either PaymentFormError ProcessorError
fromStripeError err =
    case errorParam err of
      Just field -> Left ∘ PaymentFormError $ map (review strict) [T.intercalate ": " [field, errorMsg err]]
      Nothing → Right $ ProcessorError
                (fromStripeErrorType $ errorType err)
                (review strict $ errorMsg err)
                (ErrorCode ∘ fromMaybe UnknownError $ errorCode err)
