module Happstack.Stripe.Types.Messages.Classes (
    module Happstack.Stripe.Prelude,
    module Data.Aeson,
    module Data.Aeson.Encoding,
    module Happstack.Server,
    module Happstack.Stripe.Types.JSON,
    IsPaymentError(..),
    IsPaymentMessage(..)
    )
where

import Happstack.Stripe.Prelude hiding (string, object, text,  list, (.=), lazyText)
import Data.Aeson
import Data.Aeson.Encoding
import Happstack.Stripe.Types.JSON
import Happstack.Stripe.Types.Messages.Retryable
import Happstack.Stripe.Types.Messages.PaymentErrorClass

import Happstack.Server (ToMessage(..), Response(..), toResponseBS)

class (ToJSON e, ToMessage e) => IsPaymentError e where
    peErrorClass :: Getter e PaymentErrorClass

    peHttpResponseCode ∷ Getter e Int
    peHttpResponseCode = to (\e -> e ^. peErrorClass ^. pecHttpErrorCode)

    peRetryable :: Getter e Retryable
    peRetryable = to (\e -> e ^. peErrorClass ^. pecIsRetryable)

class (ToJSON m, ToMessage m) ⇒ IsPaymentMessage m where
    pmHttpResponseCode ∷ Getter m Int
    default pmHttpResponseCode ∷ (IsPaymentError m) ⇒ Getter m Int
    pmHttpResponseCode = peHttpResponseCode
