module Happstack.Stripe.Types.TxnId (
  TxnId,
  txnId
  ) where

import Happstack.Stripe.Prelude
import Happstack.Stripe.Types.Logging
import System.Random
import Crypto.Hash
import qualified Data.ByteString as B

newtype TxnId = TxnId (Digest RIPEMD160) deriving (Show, Eq, Ord)

txnId ∷ IO TxnId
txnId = do
    rng ← newStdGen

    return $ TxnId $ hash $ B.pack $ take (512 `div` 8) $ randoms rng

instance Printable TxnId where
    print = print ∘ show

instance Loggable TxnId where
    toLogMessage t = LogMessage ["TxnId:", (show t) ^. re utf8]
