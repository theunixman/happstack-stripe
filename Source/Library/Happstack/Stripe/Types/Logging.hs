module Happstack.Stripe.Types.Logging (
    Priority(..),
    LogName(..),
    LogMessage(..),
    LogFunc,
    Loggable(..),
    toLogString) where

import Happstack.Stripe.Prelude
import Happstack.Stripe.Text()
import System.Log.Logger
import Data.String (IsString(..))
import GHC.Exts (IsList(..))
import qualified Data.Text as T
import Data.Time.Clock
import Data.Time.Format

newtype LogName = LogName [NFCText] deriving (Show, Eq, Ord)
makePrisms ''LogName

instance IsString LogName where
    fromString s =
        let
            pts = map (review strict) $ T.splitOn "." $ view strict $ review utf8 s
        in
          mempty ⊕ LogName pts

instance Monoid LogName where
    mempty = LogName ["Happstack", "Stripe"]
    a `mappend` b = LogName $ (a ^. _LogName) ⊕ (b ^. _LogName)

instance Printable LogName where
    print ln = fsep "." $ map print $ ln ^. _LogName

newtype LogMessage = LogMessage [NFCText] deriving (Show, Eq, Ord, Monoid)
makePrisms ''LogMessage

toLogString ∷ LogMessage → String
toLogString m = unwords $ map (view utf8) $ toList m

instance IsList LogMessage where
    type Item LogMessage = NFCText

    fromList = LogMessage
    toList = view _LogMessage

instance Printable LogMessage where
    print = hsep ∘ map print ∘ view _LogMessage

type LogFunc = Priority → LogMessage → IO ()

class Loggable l where
    toLogMessage ∷ l → LogMessage

instance Loggable Text where
    toLogMessage t = LogMessage [t ^.re strict]

instance Loggable NFCText where
    toLogMessage t = LogMessage [t]

instance Loggable String where
    toLogMessage s = LogMessage [s ^. re utf8]

instance Loggable ChargeId where
    toLogMessage (ChargeId chgid) = LogMessage ["Charge ID:" ^. re utf8, chgid ^. re strict]

instance Loggable UTCTime where
    toLogMessage t = LogMessage [(formatTime defaultTimeLocale (iso8601DateFormat Nothing) t) ^. re utf8]

instance Loggable Charge where
    toLogMessage chg =
        toLogMessage (chargeId chg) ⊕
        toLogMessage (chargeCreated chg)
