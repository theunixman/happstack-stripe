module Happstack.Stripe.Types.Chargeable where

import Happstack.Stripe.Prelude
import Web.Stripe
import Data.Fixed
import Data.Ratio

-- | A price in Dollars.
newtype Dollars = Dollars Centi deriving (Eq, Ord, Enum, Num, Real, Fractional, RealFrac, Generic, Read, Typeable, Data)

class
    (Num c, Real c, Fractional c, RealFrac c, Enum c, Printable c, Eq c, Ord c, Show c, Read c, Typeable c, Data c) =>
    IsCurrency c where
    toBase :: c -> Integer
    code :: c -> Currency
    symbol :: c -> Text

instance IsCurrency Dollars where
    toBase (Dollars d) = fromIntegral . numerator . toRational $ (d * (fromInteger (resolution d)))
    code _ = USD
    symbol _ = "$"

instance Show Dollars where
    show (Dollars v) =
        case v >= 0 of
            True -> concat ["$", show v]
            False -> concat ["($", show (-v), ")"]

instance Printable Dollars where
    print d = print $ hsep [show d, show . code $ d]

class (IsCurrency (ItemCurrency i)) ⇒ Chargeable i where
    type ItemCurrency i
    description :: i -> Description

    charge :: i -> StripeRequest CreateCharge
    charge a = createCharge (amount a) (currency a) -&- (description a)

    price :: i -> ItemCurrency i

    currency :: i -> Currency
    currency = const $ code (undefined ∷ (ItemCurrency i))

    amount :: i -> Amount
    amount = Amount . fromInteger . toBase . price

instance Printable Description where
    print (Description t) = print t
