{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleContexts #-}

module Happstack.Stripe.Types.PaymentForm (
    checkRq,
    PaymentForm(..),
    cfName,
    cfPhoneNumber,
    cfEmail,
    cfStreet,
    cfCity,
    cfState,
    cfZipCode,
    cfToken,
    ValidatedRequest(..),
    vrPaymentForm,
    vrItem,
    ValidatedForm,
    ValidatedItemRequest,
    Validator,
    ItemValidator,
    validateForm,
    formErrors,
    look,
    lookRead,
    parseNFCText,
    parseTokenId,
    parseEmail,
    RqData
    ) where

import Happstack.Server (ServerPartT)
import Happstack.Stripe.Prelude
import Happstack.Stripe.Types.Messages
import Happstack.Stripe.Text
import Happstack.Stripe.Types.Logging
import Happstack.Stripe.Types.Chargeable
import Happstack.Server.RqData
import qualified Data.ByteString.Char8 as C8
import qualified Text.Email.Validate as EV

-- | Payment information form
data PaymentForm = PaymentForm {
    _cfName :: NFCText,
    _cfPhoneNumber :: Integer,
    _cfEmail :: EmailAddress,
    _cfStreet :: NFCText,
    _cfCity :: NFCText,
    _cfState :: NFCText,
    _cfZipCode :: NFCText,
    _cfToken :: TokenId
}
makeLenses ''PaymentForm

data ValidatedRequest c = (Chargeable c) ⇒ ValidatedRequest
    {
        _vrPaymentForm ∷ PaymentForm,
        _vrItem ∷ c
    }

vrPaymentForm ∷ Getter (ValidatedRequest c) PaymentForm
vrPaymentForm = to _vrPaymentForm

vrItem ∷ Getter (ValidatedRequest c) PaymentForm
vrItem = to _vrPaymentForm

-- | Type for form validator function.
type ValidatedForm f  = ServerPartT IO (Either PaymentFormError f)
type ValidatedItemRequest c  = (Chargeable c) ⇒ ValidatedForm (ValidatedRequest c)

type Validator f = RqData f
type ItemValidator c = (Chargeable c) ⇒ Validator c

-- | Validate a form and return either an error or the value the form constructs.
validateForm ∷ Validator f → ValidatedForm f
validateForm validator = do
    vf <- getDataFn validator
    return $ case vf of
      Left e → Left ∘ PaymentFormError $ map (review utf8) e
      Right f → Right f

formErrors ∷ Either PaymentFormError f → PaymentFormError
formErrors = either id mempty

instance Loggable PaymentForm where
    toLogMessage cf =
        LogMessage
        [
            cf ^. cfName,
            toNFCText $ cf ^. cfPhoneNumber,
            toNFCText $ cf ^. cfEmail,
            toNFCText $ cf ^. cfZipCode
        ]

parseTokenId :: String -> Either String TokenId
parseTokenId t =
    case parseNFCText t of
        Left e -> Left e
        Right k -> Right . TokenId ∘ view strict $ k

parseNFCText :: String -> Either String NFCText
parseNFCText n =
    case decodeUtf8' . C8.pack $ n of
        Left e -> Left $ show e
        Right name -> Right ∘ review strict $ name

parseEmail ∷ String → Either String EmailAddress
parseEmail = EV.validate ∘ C8.pack
