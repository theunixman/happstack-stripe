{-# Language TemplateHaskell #-}

module Happstack.Stripe.Logging (newLogger) where

import Happstack.Stripe.Prelude
import Happstack.Stripe.Types

import System.Log.Logger

newLogger ∷ LogName → LogFunc
newLogger name =
    let
        l p m = logM (toString name) p $ toLogString m
    in
      l
