module Happstack.Stripe.Prelude (
    module Prelude,
    module Prelude.Unicode,
    module Data.Monoid.Unicode,
    module Control.Lens,
    module Data.Textual,
    module Text.Printer,
    module Data.Text.ICU.Normalized.NFC,
    module Web.Stripe.Charge,
    module Web.Stripe.Error,
    module Web.Stripe.Types,
    Getter,
    Data,
    Typeable,
    Generic(..),
    EmailAddress,
    Text,
    decodeUtf8,
    decodeUtf8'
    ) where

import Prelude hiding (print, lines)
import Prelude.Unicode
import Data.Monoid.Unicode
import Control.Lens hiding (lazy, strict, Getter)
import Data.Data (Data, Typeable)
import GHC.Generics hiding (to, from)
import Data.Text (Text)
import Data.Textual
import Text.Printer hiding (utf8)
import Web.Stripe.Types hiding (list)
import Web.Stripe.Charge hiding (list)
import Web.Stripe.Error
import Data.Text.ICU.Normalized.NFC
import Text.Email.Validate (EmailAddress)
import Data.Text.Encoding (decodeUtf8, decodeUtf8')

-- | New Lens Getter since the 4.14 Getter has a redundant constraint.
type Getter s a =
    ∀ f. (Contravariant f) => (a -> f a) -> s -> f s
