{-# Language TemplateHaskell #-}

module Happstack.Stripe.Configuration where

import Happstack.Stripe.Prelude
import Happstack.Stripe.Types
import Happstack.Stripe.LoggingHooks
import Web.Stripe.Client

data HappstackStripeConfiguration = HappstackStripeConfiguration
  {
      _hscStripePublishKey ∷ StripePublishKey,
      _hscStripeKey ∷ StripeKey,
      _hscRequestHooks ∷ [ChargeRequestHook],
      _hscSuccessHooks ∷ [ChargeSuccessHook],
      _hscFormErrorHooks ∷ [PaymentFormErrorHook],
      _hscProcessorErrorHooks ∷ [ChargeProcessorErrorHook]
  }
makeLenses ''HappstackStripeConfiguration

hscStripeConfig ∷ Getter HappstackStripeConfiguration StripeConfig
hscStripeConfig = to (StripeConfig ∘ view hscStripeKey)

happstackStripeConfiguration ∷ StripePublishKey → StripeKey → HappstackStripeConfiguration
happstackStripeConfiguration pkey key =
    HappstackStripeConfiguration pkey key [logRequestHook] [logSuccessHook] [logFormErrorHook] [logProcessorErrorHook]
