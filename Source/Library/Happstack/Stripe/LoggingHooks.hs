module Happstack.Stripe.LoggingHooks (
    logRequestHook,
    logSuccessHook,
    logFormErrorHook,
    logProcessorErrorHook
    ) where

import Happstack.Stripe.Prelude
import Happstack.Stripe.Types
import Happstack.Stripe.Logging

logger ∷ LogFunc
logger = newLogger "Charges"

logRequestHook ∷ ChargeRequestHook
logRequestHook tid cf =
    logger INFO $ mconcat [toLogMessage ("New request:" ^. re utf8), toLogMessage tid, toLogMessage cf]

logSuccessHook ∷ ChargeSuccessHook
logSuccessHook tid chg =
    logger INFO $ mconcat [toLogMessage ("Charge Success:" ^. re utf8), toLogMessage tid, toLogMessage chg]

logFormErrorHook ∷ PaymentFormErrorHook
logFormErrorHook tid pfe =
    logger INFO $ mconcat [toLogMessage ("Form Validation Error:" ^. re utf8), toLogMessage tid, toLogMessage pfe]

logProcessorErrorHook ∷ ChargeProcessorErrorHook
logProcessorErrorHook tid pe =
    logger INFO $ mconcat [toLogMessage ("Charge Error:" ^. re utf8), toLogMessage tid, toLogMessage pe]
