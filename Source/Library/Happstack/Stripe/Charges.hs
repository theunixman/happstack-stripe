module Happstack.Stripe.Charges (
    validateCaptureCharge
    ) where

import Happstack.Stripe.Prelude
import Happstack.Stripe.Types
import Happstack.Stripe.Charges.Charge
import Happstack.Stripe.Charges.PaymentForm
import Happstack.Server (toResponse, ServerPartT, Response)
import Control.Monad.IO.Class (liftIO)
import Happstack.Stripe.Configuration

-- | Validate and capture a charge, returning 'Response'.
validateCaptureCharge ∷
    ∀ c.
    (Chargeable c) ⇒
    HappstackStripeConfiguration →
    ItemValidator c → ServerPartT IO Response
validateCaptureCharge config itemv = do
    tid ← liftIO txnId
    validateForms itemv >>= \case
        Left e → do
            liftIO $ callHooks tid e (config ^. hscFormErrorHooks)
            return ∘ toResponse $ e
        Right (ValidatedRequest cf item) → do
            liftIO $ callHooks tid cf (config ^. hscRequestHooks)

            cir ← liftIO $ chargeItem config tid item cf
            case cir of
              ChargePaymentFormError pfe → do
                  liftIO $ callHooks tid pfe (config ^. hscFormErrorHooks)
              _ → return ()
            return $ toResponse cir
