module Happstack.Stripe.Text (
    normalizedStringStrict,
    normalizedStringLazy,
    fromEmail,
    toNFCText
    ) where

import Happstack.Stripe.Prelude
import qualified Data.Text.Lazy as LT
import qualified Text.Email.Validate as EV

instance Printable NFCText where
    print = print ∘ view strict

instance Printable EmailAddress where
    print = print ∘ decodeUtf8 ∘ EV.toByteString

toNFCText ∷ ∀ α. (Printable α) ⇒ α → NFCText
toNFCText = review strict ∘ toText

normalizedStringStrict ∷ String → Text
normalizedStringStrict = view strict ∘ review utf8

normalizedStringLazy ∷ String → LT.Text
normalizedStringLazy = view lazy ∘ review utf8

fromEmail ∷ EV.EmailAddress → Text
fromEmail = decodeUtf8 ∘ EV.toByteString
