{-# Language TemplateHaskell #-}

-- | Parse the charge form information. This data plus a 'Chargeable'
-- will be used to build the Stripe charge request.
module Happstack.Stripe.Charges.PaymentForm where

import Happstack.Stripe.Types
import Happstack.Stripe.Prelude

chargeForm :: Validator PaymentForm
chargeForm = PaymentForm
    <$> (look "name" `checkRq` parseNFCText)
    <*> (lookRead "phone_number")
    <*> (look "email" `checkRq` parseEmail)
    <*> (look "street_address" `checkRq` parseNFCText)
    <*> (look "city" `checkRq` parseNFCText)
    <*> (look "state" `checkRq` parseNFCText)
    <*> (look "postcode" `checkRq` parseNFCText)
    <*> (look "stripeToken" `checkRq` parseTokenId)

validateForms ∷ ItemValidator c → ValidatedItemRequest c
validateForms itemv = do
    vpf ← validateForm chargeForm
    vit ← validateForm itemv

    let errs = formErrors vpf ⊕ formErrors vit
    return $ case errs of
      PaymentFormError [] → Right $ ValidatedRequest (vpf ^?! _Right) (vit ^?! _Right)
      _ → Left errs
