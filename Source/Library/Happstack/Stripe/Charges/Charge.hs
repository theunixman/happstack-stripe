{-# Language DeriveGeneric #-}

-- | Actually charge a card, returning a JSON response.
module Happstack.Stripe.Charges.Charge where

import Happstack.Stripe.Prelude
import Happstack.Stripe.Text
import Happstack.Stripe.Types
import Happstack.Stripe.Configuration
import Web.Stripe

chargeItem ::
    (Chargeable c) =>
    HappstackStripeConfiguration -> TxnId → c -> PaymentForm -> IO ChargeResult
chargeItem config tid item form =
    let
        chg = charge item -&- (form ^. cfToken) -&- (ReceiptEmail ∘ fromEmail $ form ^. cfEmail)
    in do
        stripe (config ^. hscStripeConfig) chg >>= \case
            Left (e@StripeError{..}) → return ∘ fromStripeError $ e
            Right (c@Charge{..}) → do
                callHooks tid c (config ^. hscSuccessHooks)
                return ∘ fromCharge $ c
